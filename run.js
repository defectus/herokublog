var portNo = process.env.C9_PORT ? process.env.C9_PORT : (process.env.PORT ? process.env.PORT : 80);
var address = process.env.C9_PORT || process.env.PORT ? "0.0.0.0" : "127.0.0.1";

var connect = require('connect');

connect.createServer(
    connect.favicon(),
    connect.static(__dirname + '/public')
).listen(portNo);